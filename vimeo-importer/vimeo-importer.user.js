// ==UserScript==
// @name        MusicBrainz: Import videos from Vimeo
// @version     2014-09-08
// @author      -
// @namespace   df069240-fe79-11dc-95ff-0800200c9a66
//
// @include     *://vimeo.com/*
// ==/UserScript==
//**************************************************************************//

alert("loaded");

var myform = document.createElement("form");
myform.method="get";
myform.action = "https://musicbrainz.org/recording/create";
myform.acceptCharset = "UTF-8";
mysubmit = document.createElement("input");
mysubmit.type = "submit";
mysubmit.value = "Add to MusicBrainz";
myform.appendChild(mysubmit);

var div = document.createElement("div");
div.style.position = 'absolute';
div.style.top = 0;
div.style.right = 0;
div.style.padding = '20px';
div.style.margin = '50px';
div.style.zIndex = '10000';

var m = document.location.href.match(/\/([0-9]+)$/);

if (m && m[1]) {
	var video_id = m[1];

	var title = document.querySelectorAll("meta[property='og:title']")[0].getAttribute("content");
	var artist = document.querySelectorAll("#info meta[itemprop='name']")[0].getAttribute("content");

	var length = document.querySelectorAll("meta[property='video:duration']")[0].getAttribute("content");
	var min = Math.floor(length / 60);
	var sec = length % 60

	add_field("edit-recording.name", title);
	add_field("edit-recording.video", 1);
	add_field("edit-recording.length", min + ":" + sec);
	add_field("edit-recording.edit_note", document.location.href);
	add_field("edit-recording.artist_credit.names.0.artist.name", artist);
	add_field("edit-recording.url.0.link_type_id", "268");
	add_field("edit-recording.url.0.text", document.location.href);

	var mb_ws_url = "https://musicbrainz.org/ws/2/url/?inc=artist-rels&fmt=json&resource=http://vimeo.com/" + video_id;
	var xmlhttp2 = new XMLHttpRequest();
	xmlhttp2.open('GET', mb_ws_url, true);
	xmlhttp2.onreadystatechange = function() { mb_callback(xmlhttp2); }
	xmlhttp2.send(null);
}

function mb_callback(req) {
	if (req.readyState != 4)
		return;
	var r = eval('(' + req.responseText + ')');

	if (r.relations) {
		div.style.backgroundColor = 'lightgreen';
		div.innerHTML = "<a href='https://musicbrainz.org/url/" + r.id + "'>Already in MB</a> :D";
		document.body.appendChild(div);
	} else {
		div.style.backgroundColor = 'pink';
		div.appendChild(myform);
	}

	document.body.appendChild(div);
}

function add_field (name, value) {
	var field = document.createElement("input");
	field.type = "hidden";
	field.name = name;
	field.value = value;
	myform.appendChild(field);
}

