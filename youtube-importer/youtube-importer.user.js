// ==UserScript==
// @name        MusicBrainz: Import videos from YouTube
// @version     2014-09-07
// @author      -
// @namespace   df069240-fe79-11dc-95ff-0800200c9a66
// @require https://raw.github.com/murdos/musicbrainz-userscripts/master/lib/mbimport.js
//
// @include     *://www.youtube.com/watch?*
// ==/UserScript==
//**************************************************************************//

var google_api_key = "AIzaSyC5syukuFyCSoRvMr42Geu_d_1c_cRYouU"

var myform = document.createElement("form");
myform.method = "get";
myform.action = "//beta.musicbrainz.org/recording/create";
myform.acceptCharset = "UTF-8";
mysubmit = document.createElement("input");
mysubmit.type = "submit";
mysubmit.value = "Add to MusicBrainz";
myform.appendChild(mysubmit);

var div = document.createElement("div");
div.style.position = 'absolute';
div.style.top = 0;
div.style.right = 0;
div.style.padding = '20px';
div.style.margin = '50px';
div.style.zIndex = '10000';

var m = document.location.href.match(/\?v=([A-Za-z0-9_-]{11})/);
if (m && m[1]) {
	var yt_ws_url = "//www.googleapis.com/youtube/v3/videos?part=snippet,id,contentDetails&id=" + m[1] + "&key=" + google_api_key;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open('GET', yt_ws_url, true);
	xmlhttp.onreadystatechange = function() { yt_callback(xmlhttp); }
	xmlhttp.send(null);
}

function yt_callback(req) {
	if (req.readyState != 4)
		return;
	var r = eval('(' + req.responseText + ')').items[0];

	var video_id = r.id;
	var title = r.snippet.title;
	var artist = r.snippet.channelTitle;
	var length = MBImport.ISO8601toMilliSeconds(r.contentDetails.duration);

	add_field("edit-recording.name", title);
	add_field("edit-recording.video", 1);
	add_field("edit-recording.length", length);
	add_field("edit-recording.edit_note", document.location.href);
	add_field("edit-recording.artist_credit.names.0.artist.name", artist);
	add_field("edit-recording.url.0.link_type_id", "268");
	add_field("edit-recording.url.0.text", document.location.href);

	var mb_ws_url = "//musicbrainz.org/ws/2/url/?inc=artist-rels&fmt=json&resource=http://www.youtube.com/watch%3Fv=" + video_id;
	var xmlhttp2 = new XMLHttpRequest();
	xmlhttp2.open('GET', mb_ws_url, true);
	xmlhttp2.onreadystatechange = function() { mb_callback(xmlhttp2); }
	xmlhttp2.send(null);
}

function mb_callback(req) {
	if (req.readyState != 4)
		return;
	var r = eval('(' + req.responseText + ')');

	if (r.relations) {
		div.style.backgroundColor = 'lightgreen';
		div.innerHTML = "<a href='//musicbrainz.org/url/" + r.id + "'>Already in MB</a> :D";
		document.body.appendChild(div);
	} else {
		div.style.backgroundColor = 'pink';
		div.appendChild(myform);
	}

	document.body.appendChild(div);
}

function add_field (name, value) {
	var field = document.createElement("input");
	field.type = "hidden";
	field.name = name;
	field.value = value;
	myform.appendChild(field);
}

